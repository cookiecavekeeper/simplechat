var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/html/index.html');
});


io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('chat message', function(obj){
        io.sockets.emit('chat message', obj);
    });

    socket.on('command', function(obj) {
        var objResponse = {
            username: 'Server',
            message: command(socket, obj.command, obj.options),
            color: 'red'
        };
        socket.emit('chat message', objResponse);
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});

function command(socket, strCommand, strOptions) {
    var strReturn = '';
    switch(strCommand) {
        case 'help':
            strReturn = 'You wont get any help here';
            break;

        default:
            strReturn = 'ERROR: Command not found!';
            break;
    }
    return strReturn;
}