var App = function() {

    var $this = this;
    
    var socket = io();
    
    var user = {
        username: '',
        color: ''
    };
    
    this.init = function() {
        $this.registerSocket();
        $this.initUser();
        
        $('#message-form').on('submit', $this.sendMessage);
        $('#set-username').on('click', $this.setUsername);

        $('#message').focus();
    };
    
    this.registerSocket = function() {
        socket.on('chat message', $this.postMessage);
    };
    
    this.initUser = function() {
        user.username = localStorage.username || '';
        user.color = Please.make_color();
        $('#username').val(user.username)
    };
    
    this.setUsername = function() {
        localStorage.username = $('#username').val();
    };
    
    this.postMessage = function(data) {
        var template = Handlebars.compile($("#message-template").html());
        var params = {
            username: data.username,
            message: data.message,
            color: data.color
        };
        var strHtml = template(params);

        $('.chat-log').prepend(strHtml);
    };
    
    this.sendMessage = function(e) {
        e.preventDefault();
        var data = {
            username: $('#username').val(),
            message: $('#message').val(),
            color: user.color
        };

        if (!data.message)
            return;

        var regexCommand = /^\/((\w+)(?:\s(.*))?)/;

        var arrCommand = data.message.match(regexCommand);

        if(arrCommand !== null) {
            // Command
            data.command = arrCommand[2];
            data.options = arrCommand[3];
            socket.emit('command', data);
        } else {
            // Message
            socket.emit('chat message', data);
        }
        
        $('#message').val('');
    };

    this.init();

};

$(function() {
    new App();
});