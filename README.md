SimpleChat
==========

A simple Chat Application, powered by Node, Socket.io and some other stuff.

## Installation

- Install [Node.js](http://www.nodejs.org)
- clone repo into local directory `git clone https://cookiecavekeeper@bitbucket.org/cookiecavekeeper/simplechat.git simplechat`
- run `cd simplechat`
- run `npm install`
- run `node index.js`
- navigate to `http://127.0.0.1:3000`
- success